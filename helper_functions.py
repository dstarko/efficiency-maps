import glob
import os
import time
import sys
import ROOT
from array import array
import json

from array import array
import math

def getOrMkdir (dir, name):
    dirName = dir.Get(name)
    if dirName:
        return dirName
    return dir.mkdir(name)

def create_3d_hist(hists,tagweightbinning,ptbinning,etabinning):

    h_3d = ROOT.TH3D('eff_cont','eff_cont',len(ptbinning)-1,array('f',ptbinning),
        len(etabinning)-1,array('f',etabinning),
        len(tagweightbinning)-1,array('f',tagweightbinning))

    for tagweight_bin in range(1,len(tagweightbinning)):
        for pt_i in range(1,len(ptbinning)):
            for eta_i in range(1,len(etabinning)):
                bin_val = hists[tagweight_bin-1].GetBinContent(pt_i,eta_i)
                bin_err = hists[tagweight_bin-1].GetBinError(pt_i,eta_i)

                h_3d.SetBinContent(pt_i,eta_i,tagweight_bin,bin_val)
                h_3d.SetBinError(pt_i,eta_i,tagweight_bin,bin_err)
    return h_3d

def rebin2D(histtype,hist,ptbinning,etabinning):

    hnew = ROOT.TH2F('rebinned_'+hist.GetName()+'_'+histtype,'rebinned_'+hist.GetName()+'_'+histtype,
        len(ptbinning)-1,array('f',ptbinning),len(etabinning)-1,array('f',etabinning))


    hnew_err = ROOT.TH2F('err','err',len(ptbinning)-1,array('f',ptbinning),len(etabinning)-1,array('f',etabinning))

    xaxis = hist.GetXaxis()
    yaxis = hist.GetYaxis()

    for j in range(1,yaxis.GetNbins()+1):
        for i in range(1,xaxis.GetNbins()+1):

            ptval = xaxis.GetBinCenter(i)
            etaval = yaxis.GetBinCenter(j)

            if hist.GetBinContent(i,j) < 0:
                continue

            if ptbinning[0] <= ptval < ptbinning[-1] and etabinning[0] <= etaval < etabinning[-1]:
                hnew.Fill(ptval,etaval,hist.GetBinContent(i,j))
                hnew_err.Fill(ptval,etaval,hist.GetBinError(i,j)**2)

    for j in range(1,hnew.GetYaxis().GetNbins()+1):
        for i in range(1,hnew.GetXaxis().GetNbins()+1):
            hnew.SetBinError(i,j, math.sqrt(hnew_err.GetBinContent(i,j)) )

    return hnew

def check_for_empty_bins(hist):
    #checks the bins in the last column and before last column
    #for empty bins

    last_column = hist.GetNbinsX()
    before_last_col = last_column-1
    n_y_bins = hist.GetNbinsY()

    for ybin in range(1,n_y_bins+1):
        for xbin in [before_last_col,last_column]:
            binval = hist.GetBinContent(xbin,ybin)

            if binval <= 0:
                if xbin == last_column:
                    return True, 'last_column'
                else:
                    return True, 'before_last_col'

    return False, 'ok'


def getEfficiency(ptbinning,etabinning,totalhist,passedhist):
    rebinned_passed = rebin2D('test_pass',passedhist,ptbinning,etabinning)

    empty, which_empty = check_for_empty_bins(rebinned_passed)

    if empty:
        print 'empty bin! '+which_empty
        return False, which_empty

    rebinned_total = rebin2D('test_total',totalhist,ptbinning,etabinning)
    eff = rebinned_total.Clone('eff')
    eff.Divide(rebinned_passed, rebinned_total,1.0,1.0,"B")
    return eff, 'ok'

def getWeights(default_hist,target_hist):
    weights = default_hist.Clone('weights')

    nBins_eta = default_hist.GetNbinsY()
    nBins_pt = default_hist.GetNbinsX()

    sum_default = default_hist.GetSumOfWeights()
    sum_reweight = target_hist.GetSumOfWeights()


    target_clone = target_hist.Clone('target_clone')
    target_clone.Scale(float(sum_default)/sum_reweight)

    for ipt in range(1,nBins_pt+1):
        for ieta in range(1,nBins_eta+1):

            binContDef = float(default_hist.GetBinContent(ipt,ieta))
            binContReweight = float(target_clone.GetBinContent(ipt,ieta))

            frac_default = binContDef/float(sum_default)
            frac_reweight = binContReweight/float(sum_default) #float(sum_reweight)

            if frac_default < 0.0001 or  frac_reweight < 0.0001:
                Weight = 1
                WeightErr = 0
            else:
                Weight = frac_default/frac_reweight

                ### formula for error of (x1*(n2 + x2))/(x2*(n1 + x1)) where the variables are:
                x1 = binContDef
                x2 = binContReweight
                n1 = sum_default - binContDef
                n2 = sum_default - binContReweight

                WeightErr = math.sqrt(x1*(n2+x2)*( n1*(n2+x2)/(n1+x1)+x1*n2/x2 ))/(n1+x1)/x2


            weights.SetBinContent(ipt,ieta,Weight)
            weights.SetBinError(ipt,ieta,WeightErr)

    return weights



def applyWeights(weights,hist):

    reweighted = hist.Clone(hist.GetName()+'_reweighted')
    #print 'Made it here'
    nBins_eta = weights.GetNbinsY()
    nBins_pt = weights.GetNbinsX()

    for ipt in range(1,nBins_pt+1):
        for ieta in range(1,nBins_eta+1):
            reweighted.SetBinContent(ipt,ieta,weights.GetBinContent(ipt,ieta)*hist.GetBinContent(ipt,ieta))
            #print str(weights.GetBinContent(ipt,ieta))+' '+str(hist.GetBinContent(ipt,ieta))
            errPart1 = (weights.GetBinContent(ipt,ieta)*hist.GetBinError(ipt,ieta))**2
            errPart2 = (weights.GetBinError(ipt,ieta)*hist.GetBinContent(ipt,ieta))**2

            reweighted.SetBinError(ipt,ieta, math.sqrt(errPart1+errPart2))
#             print 'pt ',reweighted.GetXaxis().GetBinCenter(ipt),' eta ',reweighted.GetYaxis().GetBinCenter(ieta), \
#                 ' before ',hist.GetBinContent(ipt,ieta) ,' reweight ' \
#                 ,reweighted.GetBinContent(ipt,ieta),' err ',reweighted.GetBinError(ipt,ieta)
            if reweighted.GetBinContent(ipt,ieta)!=0:
                ratio = reweighted.GetBinError(ipt,ieta)/reweighted.GetBinContent(ipt,ieta)
                #print str(hist.GetName())+' '+str(ipt)+' '+str(ieta)+' '+str(ratio)
    return reweighted



def write_outputfile(outputname,hist,tagger,jetCol,wp,flav,bin,sample,histname):
    outfile = ROOT.TFile(outputname,'update')
    tagger_dir = getOrMkdir(outfile,tagger)
    jetcol_dir = getOrMkdir(tagger_dir,jetCol)
    wp_dir = getOrMkdir(jetcol_dir,wp)
    flav_dir = getOrMkdir(wp_dir,flav)
    bin_dir = getOrMkdir(flav_dir,bin)
    sample_dir = getOrMkdir(bin_dir,sample)
    sample_dir.WriteTObject(hist,histname)
    outfile.Close()

def write_outputfile_tag(outputname,hist,tagger,jetCol,wp,flav,bin,tag,sample,histname):
    outfile = ROOT.TFile(outputname,'update')
    tagger_dir = getOrMkdir(outfile,tagger)
    jetcol_dir = getOrMkdir(tagger_dir,jetCol)
    wp_dir = getOrMkdir(jetcol_dir,wp)
    flav_dir = getOrMkdir(wp_dir,flav)
    bin_dir = getOrMkdir(flav_dir,bin)
    tag_dir = getOrMkdir(bin_dir,tag)
    sample_dir = getOrMkdir(tag_dir,sample)
    sample_dir.WriteTObject(hist,histname)
    outfile.Close()

def write_outputfile_reg(outputname,hist,tagger,jetCol,wp,flav,sample,histname):
    outfile = ROOT.TFile(outputname,'update')
    tagger_dir = getOrMkdir(outfile,tagger)
    jetcol_dir = getOrMkdir(tagger_dir,jetCol)
    wp_dir = getOrMkdir(jetcol_dir,wp)
    flav_dir = getOrMkdir(wp_dir,flav)
    sample_dir = getOrMkdir(flav_dir,sample)
    sample_dir.WriteTObject(hist,histname)
    outfile.Close()

def write_outfile(outputname,hist,histname,*args):
    outfile = ROOT.TFile(outputname,'update')
    old_dir = outfile
    for arg in args:
        new_dir = getOrMkdir(old_dir,arg)
        old_dir = new_dir
    old_dir.WriteTObject(hist,histname)
    outfile.Close()
    
def makedir(dir, name = ''):
    dir_name = dir+'/'+name
    if os.path.exists(dir_name):
        return dir_name
    os.mkdir(dir_name)
    return dir_name

def save_plot_reg(plot,main,tagger,jetCol,wp,flav,sample,plotname):
    tagger_dir = makedir(main,tagger)
    jetcol_dir = makedir(tagger_dir,jetCol)
    wp_dir = makedir(jetcol_dir,wp)
    flav_dir = makedir(wp_dir,flav)
    sample_dir = makedir(flav_dir,sample)
    plot.SaveAs(sample_dir+'/'+plotname+'.png')

def save_plot_reg_cont(plot,main,tagger,jetCol,wp,flav,twbin,plotname):
    tagger_dir = makedir(main,tagger)
    jetcol_dir = makedir(tagger_dir,jetCol)
    wp_dir = makedir(jetcol_dir,wp)
    flav_dir = makedir(wp_dir,flav)
    twbin_dir = makedir(flav_dir,twbin)
    plot.SaveAs(flav_dir+'/'+plotname+'.png')

def save_plot(plot,main,tagger,jetCol,wp,flav,bin,plotname):
    tagger_dir = makedir(main,tagger)
    jetcol_dir = makedir(tagger_dir,jetCol)
    wp_dir = makedir(jetcol_dir,wp)
    flav_dir = makedir(wp_dir,flav)
    bin_dir = makedir(flav_dir,bin)
    plot.SaveAs(bin_dir+'/'+plotname+'.png')

def save_plot_cont(plot,main,tagger,jetCol,wp,flav,bin,twbin,plotname):
    tagger_dir = makedir(main,tagger)
    jetcol_dir = makedir(tagger_dir,jetCol)
    wp_dir = makedir(jetcol_dir,wp)
    flav_dir = makedir(wp_dir,flav)
    bin_dir = makedir(flav_dir,bin)
    twbin_dir = makedir(bin_dir,twbin)
    plot.SaveAs(twbin_dir+'/'+plotname+'.png')

def saver(plot,plotname,main,*args):
    old_dir = makedir(main)
    for arg in args:
        new_dir = makedir(old_dir,arg)
        old_dir = new_dir
    plot.SaveAs(old_dir+'/'+plotname+'.png')

import ROOT


def lowpt(jetcol):

	lowptdict = {
	# 'AntiKt4EMPFlowJets' : 20,
	# 'AntiKt4EMTopoJets' : 20,
	# 'AntiKt2PV0TrackJets': 10,
	 'AntiKtVR30Rmax4Rmin02TrackJets': 10,
	 'AntiKtVR30Rmax4Rmin02TrackJets_BTagging201903': 10,
	'AntiKt4EMPFlowJets_BTagging201903' : 20
	}

	return lowptdict[jetcol]

def findhighpt(totalHists,passedHists):

	for upper_bound in [400+i*100 for i in range(12)]:
		for t_h, p_h in zip(totalHists, passedHists):
			p_counts = get_column([upper_bound-100,upper_bound],p_h,p_h,'x',[0,2.5])[0]
			t_counts = get_column([upper_bound-100,upper_bound],t_h,t_h,'x',[0,2.5])[0]
			if t_counts < 300 or p_counts < 100:
				return upper_bound-100

	return 1700

def find_pt_threshold(totalHists,etabinning,pt_step,lowpt):

	pt_threshold = 10000

	for t_h in totalHists:
		col = get_column([lowpt,lowpt+pt_step],t_h,t_h,'x',etabinning)
		pt_threshold = min([pt_threshold,min(col)])
	return pt_threshold

def get_column(boundries,p_h,t_h,ax_name,perp_bins):
	#ROOT version
	col = []

	low, high = boundries
	for perp_i in range(len(perp_bins)-1):
		perp_low = perp_bins[perp_i]
		perp_high = perp_bins[perp_i+1]

		tcut = ROOT.TCutG('tcut',4)
		if ax_name == 'x':
			tcut.SetPoint(0,low,perp_low)
			tcut.SetPoint(1,low,perp_high)
			tcut.SetPoint(2,high,perp_high)
			tcut.SetPoint(3,high,perp_low)
		elif ax_name == 'y':
			tcut.SetPoint(0,perp_low,low)
			tcut.SetPoint(1,perp_low,high)
			tcut.SetPoint(2,perp_high,high)
			tcut.SetPoint(3,perp_high,low)
		counts = tcut.IntegralHist(t_h)
		counts_pass = tcut.IntegralHist(p_h)
		if counts_pass < 1:
			counts = 0
		col.append(counts)

	return col



def check_column(boundries,p_hists,t_hists,ax_name,perp_bins,threshold):

	n_samples = len(p_hists)

	for sample_i in range(n_samples):
		p_h_i = p_hists[sample_i]
		t_h_i = t_hists[sample_i]

		col = get_column(boundries,p_h_i,t_h_i,ax_name,perp_bins)

		for cell in col:
			if cell < threshold:

				return 0
	return 1


def get_columns_status(c1,c2,ax_name,passedHists,totalHists,perp_bins,threshold):

	col1 = check_column(c1,passedHists,totalHists,ax_name,perp_bins,threshold)
	col2 = check_column(c2,passedHists,totalHists,ax_name,perp_bins,threshold)

	return col1,col2

def find_binning(low,high,min_size,ax_name,passedHists,totalHists,perp_bins,threshold=1000):

	nsamples = len(passedHists)

	bins = [low,round(low+min_size,2),high]

	while (True):

		beforelast = [bins[-3],bins[-2]]
		last = [bins[-2],bins[-1]]

		c1_status, c2_status = get_columns_status(beforelast,last,ax_name, passedHists,totalHists,perp_bins,threshold)
		print 'c1_status: ',c1_status,' c2_status: ',c2_status
		if (c1_status==0 and c2_status==0) or  (c1_status==1 and c2_status==0):
			bins.pop(-2)
			break
		if (c1_status==0 and c2_status==1):
			if( (bins[-1]-bins[-2]) < min_size*2):
				bins.pop(-2)
				break
			bins[-2] = bins[-2]+min_size
			continue
		if (c1_status==1 and c2_status==1):
			if( (bins[-1]-bins[-2]) < min_size*2):
				break
			bins.insert(-1,round(bins[-2]+min_size,2))

	return bins



def findPtEtaBinning(flav,jetcol,passedHists,totalHists,eta_threshold=2000000,
	eta_range=[0,2.5],eta_step=0.2,pt_range=-1,pt_step=10):


	# if flav=='B':
	# 	eta_threshold_factor = 8
	# 	pt_threshold_factor = 20
	# elif flav=='C':
	# 	eta_threshold_factor = 8
	# 	pt_threshold_factor = 2.0
	# elif flav=='Light':
	# 	eta_threshold_factor = 7
	# 	pt_threshold_factor = 0.1
	# elif flav=='T':
	# 	eta_threshold_factor = 8
	# 	pt_threshold_factor = 2.0




	eta_threshold = min(  [get_column([0,eta_step],x,x,'y',[0,5000000])[0] for x in totalHists] )
	#min([x.GetSumOfWeights() for x in totalHists])/eta_threshold_factor


	print 'eta_threshold ',eta_threshold
	n_pt_bins = 0

	low_pt = lowpt(jetcol)

	#highpt = findhighpt(totalHists,passedHists)
	highpt = 400
	pt_threshold_factor = 1

	n_iterations = 0

	while n_pt_bins < 5:

		n_iterations+=1

		etabinning = find_binning(eta_range[0],eta_range[1],eta_step,'y',passedHists,totalHists,
								  [0,5000000],threshold=eta_threshold)

		print 'etabinning ',etabinning




		pt_threshold =  min(  [get_column([low_pt,low_pt+1],x,x,'x',[etabinning[0],etabinning[1]])[0] for x in totalHists] ) #find_pt_threshold(totalHists,etabinning,pt_step,low_pt)


		pt_threshold = pt_threshold*pt_threshold_factor
		print ' pt_threshold ',pt_threshold
		if pt_threshold < 10:
			pt_threshold = 10

		if pt_range!=-1:
			low_pt = pt_range[0]
			highpt = pt_range[1]

		ptbinning = find_binning(low_pt,highpt,pt_step,'x',passedHists,totalHists,etabinning,threshold=pt_threshold)

		n_pt_bins = len(ptbinning)-1

		print 'n_pt_bins ',n_pt_bins
		if len(etabinning) > 2:
			eta_threshold = eta_threshold*1.5
		else:
			pt_threshold_factor = pt_threshold_factor*0.9

		if n_iterations > 9:
			break
	#print pt_step
	return ptbinning,etabinning

def findBinningCalibBinned(calib_bins_pt, calib_bins_eta,jetcol,h_total):

	ptbins_full = []
	eta_bins_full = []

	for eta_bin_i in range(len(calib_bins_eta)-1):
		eta_low,eta_high = calib_bins_eta[eta_bin_i], calib_bins_eta[eta_bin_i+1]

		eta_step=0.2
		pt_range=[calib_bins_pt[0],calib_bins_pt[-1]]
		etabinning = find_binning(eta_low,eta_high,eta_step,
								'y',h_total,h_total,
							  pt_range,threshold=500000)

		eta_bins_full+=etabinning

	eta_bins_full = list(set(eta_bins_full))


	for pt_bin in range(len(calib_bins_pt)-1):
		pt_low,pt_high = calib_bins_pt[pt_bin], calib_bins_pt[pt_bin+1]

		pt_step = 5
		ptbinning = find_binning(pt_low,pt_high,pt_step,
			'x',h_total,h_total,eta_bins_full,threshold=300)

		ptbins_full+= ptbinning

	ptbins_full = list(set(ptbins_full))

	ptbins_full.sort()
	eta_bins_full.sort()

	return ptbins_full,eta_bins_full

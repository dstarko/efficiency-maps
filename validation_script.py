# Script to validate pseudo-continuous calibrations directly from the b-tagging official software

# import section
import ROOT
import os
import ctypes
import argparse
import sys
import json
from labels import *
# setup xAOD
ROOT.xAOD.Init().ignore()

# store
store = ROOT.xAOD.TStore()

# load atlas style
ROOT.gROOT.ProcessLine('.L atlasstyle/AtlasStyle.C')
ROOT.gROOT.ProcessLine('.L atlasstyle/AtlasLabels.C')

# ROOT.gROOT.SetStyle('ATLAS')
ROOT.SetAtlasStyle()

# parser
parser = argparse.ArgumentParser()
parser.add_argument('--batch', action = 'store_true')
parser.add_argument('--newConfig', action = 'store_true')
args = parser.parse_args()

ROOT.gROOT.SetBatch(ROOT.kFALSE)

jetCols, taggers, workingPoints, flavours, sample_names, twbins = labels()
# global variables
flavours_ls = {'B' : 5,
            'C' : 4,
            'Light' : 0,
            'T' : 15,}

tagweight_binning = {'0' : '100% < tagweight < 85%',
                     '1' : '85% < tagweight < 77%',
                     '2' : '77% < tagweight < 70%',
                     '3' : '70% < tagweight < 60%',
                     '4' : 'tagweight < 60%',
        }
nominal_sample_id = sample_ids['PhPy8EG'][0]

# main
if __name__ == '__main__':
    sfs = {} #Initialize dictionary for SF
    mc_sf = {} #Initialize dictionary for MC/MC SF
    configfilename = 'MC-MC_SF_TEST.json'
    cdi = '/afs/cern.ch/work/f/fdibello/public/2020-21-13TeV-MC16-CDI_smooth.root' #Input smooth CDI file

    for tagger in taggers:
        sfs[tagger] = {} #Expand dictionary
        mc_sf[tagger] = {} #Expand dictionary
        for jet in jetCols:
            sfs[tagger][jet] = {} #Expand dictionary
            mc_sf[tagger][jet] = {} #Expand dictionary
            for flavour in flavours:
                sfs[tagger][jet][flavour] = {} #Expand dictionary
                mc_sf[tagger][jet][flavour] = {} #Expand dictionary
                ptbinning,etabinning = binning(flavour,bins = 'calibrated')
                for wp in workingPoints:
                    sfs[tagger][jet][flavour][wp] = {} #Expand dictionary
                    mc_sf[tagger][jet][flavour][wp] = {} #Expand dictionary
                    for sample in sample_names:
                        sample_id = sample_ids[sample][0] #Sample id for sample name
                        print "%s %s %s %s %s"%(tagger,jet,flavour,wp,sample_id)
                        tool_name = '%s_%s_%s_%s_%s'%(tagger,jet,flavour,wp,sample_id)
                        print
                        # b-tagging tool
                        btagtool = ROOT.BTaggingEfficiencyTool(tool_name)
                        btagtool.setProperty('ScaleFactorFileName',cdi)
                        btagtool.setProperty("TaggerName", tagger)
                        btagtool.setProperty("OperatingPoint", wp)
                        btagtool.setProperty("JetAuthor", jet)
                        btagtool.setProperty("MinPt", 20000)
                        btagtool.setProperty('Efficiency'+flavour+'Calibrations',sample_id)
                        # btagtool.setProperty('ScaleFactorBCalibration',flavour)

                        btagtool.initialize()
                        sfs[tagger][jet][flavour][wp][sample_id] = {} #Expand dictionary
                        mc_sf[tagger][jet][flavour][wp][sample_id] = {} #Expand dictionary
                        print '%s_%s_%s_%s_%s'%(tagger,jet,wp,flavour,sample_id)
                        # dummy jet
                        jet_vars = ROOT.Analysis.CalibrationDataVariables()
                        for eta in etabinning:
                            sfs[tagger][jet][flavour][wp][sample_id][eta] = {} #Expand dictionary
                            mc_sf[tagger][jet][flavour][wp][sample_id][eta] = {} #Expand dictionary
                            for pt in ptbinning:
                                name = '%s_%s_%s_%s'%(tagger,jet,wp,flavour)
                            # jettagweights =['0','1','2','3','4']
                                sf = ctypes.c_float()
                                jet_vars.jetPt = pt * 1e3
                                # jet_vars.jetTagWeight = float(tagweight_name[jetTagWeight])
                                jet_vars.jetEta = eta

                                btagtool.getScaleFactor(flavours_ls[flavour],jet_vars, sf)
                                sfs[tagger][jet][flavour][wp][sample_id][eta][pt] = sf.value #Store SF
                                print "Pt: %.2f eta: %.2f SF (%s): %.4f"%(jet_vars.jetPt,jet_vars.jetEta, sample,float(sf.value))
                    for sample in sample_names:
                        for eta in etabinning:
                            for pt in ptbinning:
                                sample_id = sample_ids[sample][0]
                                sf_default = sfs[tagger][jet][flavour][wp][nominal_sample_id][eta][pt] #Obtain nominal SF
                                sf_alt = sfs[tagger][jet][flavour][wp][sample_id][eta][pt] #Obtain SF
                                print '%s_%s_%s_%s_%s_%s  Nominal:  %.4f'%(tagger,flavour,wp,nominal_sample_id,eta,pt,sf_default)
                                print '%s_%s_%s_%s_%s_%s  Sample:  %.4f'%(tagger,flavour,wp,sample_id,eta,pt,sf_alt)
                                mc_sf[tagger][jet][flavour][wp][sample_id][eta][pt] = sf_default/sf_alt #Compute and store MC/MC SF
                    #Make copy
    config_file = open(configfilename,'w')
    config_file.write(json.dumps(mc_sf,indent=3,sort_keys=True))

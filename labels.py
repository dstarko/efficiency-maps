import itertools
import os
import sys


def labels():
    categories = ['maps_a','maps_d','maps_e']
    jetCols = ['AntiKtVR30Rmax4Rmin02TrackJets_BTagging201903']
    taggers = ['DL1','DL1r','DL1rmu']
    workingPoints = ['FixedCutBEff_'+x for x in ['60','70','77','85']]+['Continuous']
    flavours = ['B','C','L','T']
    sample_names = ['PhPy8EG','Sherpa221','PowhegHerwig7','PowhegHerwig713','aMcAtNloPy8EvtGen','AtNloHerwig7','Sh_N30NNLO']
    twbins = {
    'DL1':['[-100-0.785000026226]','[0.785000026226-2.375]','[2.375-3.36500000954]','[3.36500000954-4.625]','[4.625-100]'],
    'DL1r':['[-100-1.08500003815]','[1.08500003815-2.58500003815]','[2.58500003815-3.5150001049]','[3.5150001049-4.80499982834]','[4.80499982834-100]'],
    'DL1rmu':['[-100-1.17499995232]','[1.17499995232-2.52500009537]','[2.52500009537-3.45499992371]','[3.45499992371-4.68499994278]','[4.68499994278-100]']
    }
    return jetCols, taggers, workingPoints, flavours, sample_names, twbins

def binning(flav,bins = 'calibrated'):
    if bins == 'calibrated':
    	if flav == 'L':
    		ptbinning,etabinning = [[10.0,30.0,60.0,150.0,300.0],[0,2.5]]
    	elif flav == 'B':
    		ptbinning,etabinning = [[10.0,20.0,30.0,60.0,100.0,250.0],[0,2.5]]
    	elif flav == 'C':
    		ptbinning,etabinning = [[10.0,20.0,40.0,65.0,140.0],[0,2.5]]
    	else:
    		ptbinning,etabinning = [[10.0,20.0,40.0,65.0,140.0],[0,2.5]]
    elif bins == 'finely':
    	if flav == 'L':
    		ptbinning,etabinning = [10,20,30,40,60,80,110,140,160,200,400],[0.0,1,2.5]
    	elif flav == 'T':
    		ptbinning,etabinning = [10,20,30,40,60,80,110,140,160,240,400],[0.0,1,2.5]
    	else:
    		ptbinning,etabinning = [10,20,30,40,60,80,110,140,160,200,260,320,400],[0.0,1,1.5,2.5]
    return ptbinning,etabinning

def data_extract():
    datasets = {
    'PhPy8EG':'PhPy8EG/PhPy8EG.output.root',
    'Sherpa221':'Sherpa221/Sherpa221.output.root',
    'PowhegHerwig7':'PowhegHerwig7/PowhegHerwig7.output.root',
    'PowhegHerwig713':'PowhegHerwig713/PowhegHerwig713.output.root',
    'aMcAtNloPy8EvtGen':'aMcAtNloPy8EvtGen/aMcAtNloPy8EvtGen.output.root',
    'AtNloHerwig7':'AtNloHerwig7/AtNloHerwig7.output.root',
    'Sh_N30NNLO':'Sh_N30NNLO/Sh_N30NNLO.output.root'
    }
    return datasets
sample_ids = {
'PhPy8EG':['410470','Pythia8EvtGen'],
'Sherpa221':['410250','Sherpa'],
'PowhegHerwig7':['410558','Herwig7'],
'PowhegHerwig713':['411233','Herwig713'],
'aMcAtNloPy8EvtGen':['410464','Py8_aMCNLO'],
'AtNloHerwig7':['412116','AtNloHerwig7'],
'Sh_N30NNLO':['421152','Sherpa228']
}
